import sys
sys.path.append('../../')

import os
import pickle
from base.base_module import BaseModule
import torch.nn as nn
import torch
from torchsummary import summary
import numpy as np
from common import SNAP_PARENT
from utils.util import *
from utils.loss import *
from utils.optimizer import *

class SIKNet(BaseModule):
    def __init__(self):
        super().__init__()
        self.epoch = 50
        self.train_batch_size = 256
        self.val_batch_size = 256

    def define_dataset(self):
        class SIK1M(torch.utils.data.Dataset):
            def __init__(self, ):
                root = "/Users/coreqode/Desktop/projects/datasets/"
                with open(os.path.join(root, "SIK-1M/sik-1m.pkl"),'rb') as filepath:
                    self.data = pickle.load(filepath)
                    filepath.close()
                self.jointsR = self.data['joint_']
                self.quats = self.data['quat']
                self.ref_bone_link = (0, 9)

            def __getitem__(self, idx):
                jointR = self.jointsR[idx]
                quat = self.quats[idx]
                joint_bone = 0
                for j, nextj in zip(self.ref_bone_link[:-1], self.ref_bone_link[1:]):
                    joint_bone += np.linalg.norm(jointR[nextj] - jointR[j])
                joint_bone = np.array(joint_bone)
                jointRS = jointR / joint_bone
                kin_chain = []
                for i in range(21):
                    kin_chain.append([jointRS[i]- jointRS[SNAP_PARENT[ i ]]])
                kin_chain = np.concatenate(kin_chain, axis = 0)
                kin_chain = np.array(kin_chain[1:])
                kin_chain_len = np.linalg.norm(kin_chain, ord = 2, axis = 1, keepdims = True)

                model_inputs  = {
                    'kin_chain' : torch.from_numpy(kin_chain).float(),
                }
                data  = {
                    'joint_bone' : torch.from_numpy(joint_bone).float(),
                    'jointRS' : torch.from_numpy(jointRS).float(),
                    'quat' : torch.from_numpy(quat).float(),
                    'kin_chain' : torch.from_numpy(kin_chain).float(),
                    'kin_chain_len' : torch.from_numpy(kin_chain_len).float(),
                }
                return model_inputs, data

            def __len__(self):
                return self.jointsR.shape[0]

        dataset  = SIK1M()
        self.train_dataset, self.val_dataset = torch.utils.data.random_split(dataset, [int(0.8 * len(dataset)), int(0.2 * len(dataset))])


    def define_model(self):
        class IKNet(nn.Module):
            def __init__(
                self,
                njoints=20,
                hidden_size_pose=[256, 512, 1024, 1024, 512, 256],
            ):
                super().__init__()
                self.njoints = njoints
                in_neurons = 3 * njoints
                out_neurons = 16 * 4  # 16 quats
                neurons = [in_neurons] + hidden_size_pose

                invk_layers = []
                for layer_idx, (inps, outs) in enumerate(zip(neurons[:-1], neurons[1:])):
                    invk_layers.append(nn.Linear(inps, outs))
                    invk_layers.append(nn.BatchNorm1d(outs))
                    invk_layers.append(nn.ReLU())

                invk_layers.append(nn.Linear(neurons[-1], out_neurons))

                self.invk_layers = nn.Sequential(*invk_layers)

            def forward(self, model_inputs):
                joint = model_inputs['kin_chain']
                joint = joint.contiguous().view(-1, self.njoints*3)
                quat = self.invk_layers(joint)
                quat_ = quat.view(-1, 16, 4)
                quat = normalize_quaternion(quat_)
                so3 = quaternion_to_angle_axis(quat).contiguous()
                so3 = so3.view(-1, 16 * 3)
                out = {'so3': so3, 'quat':quat, 'unnorm_quat': quat_}
                return out

        self.model = IKNet()

    def loss_func(self, data, predictions):
        ###### Quat Loss ######
        l2 = mse_loss(predictions['quat'], data['quat'])
        inv_predquat = quaternion_inv(predictions['quat'])
        real_part = quaternion_mul(data['quat'], inv_predquat)[..., 0]
        cos_loss = l1_loss(real_part, torch.ones_like(real_part))
        quat_norm = torch.norm(predictions['unnorm_quat'], dim=-1, keepdim=True)
        norm_loss = mse_loss(quat_norm, torch.ones_like(quat_norm))
        return {   'norm_loss': 100 * norm_loss,
                  'l2_loss': l2,
                  'cos_loss': cos_loss}

    def define_optimizer(self):
        self.optimizer = Adam(self.model.parameters(), self.learning_rate)

def main():
    ht = SIKNet()
    ht.init(wandb_log = False, project ='handtailor', entity = 'noldsoul')
    ht.train()


if __name__ == "__main__":
    main()
