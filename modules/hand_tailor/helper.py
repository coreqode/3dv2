
import torch
import cv2
import numpy as np
import random
import torchvision
from common import *

def voxel_to_uvd(hm3d):
    b, c, w, h = hm3d.size()
    hm2d = hm3d[:,:21,...]
    depth = hm3d[:,21:,...]
    uv = heatmap_to_keypoint2d(hm2d)/w
    hm2d = hm2d.view(b,1,c//2,-1)
    depth = depth.view(b,1,c//2,-1)
    hm2d = hm2d / torch.sum(hm2d,-1,keepdim=True)
    d = torch.sum(depth * hm2d,-1).permute(0,2,1)
    joint = torch.cat((uv,d),dim=-1)
    return joint

def uvd_to_xyz(uvd, joint_root, joint_bone, intr=None, trans=None, scale=None, inp_res=256, mode='persp'):
    bs = uvd.shape[0]
    uv = uvd[:, :, :2] * inp_res # 0~256
    depth = ( uvd[:, :, 2] * DEPTH_RANGE  ) + DEPTH_MIN
    root_depth = joint_root[:, -1].unsqueeze(1) #(B, 1)
    z = depth * joint_bone.expand_as(uvd[:, :, 2]) + \
        root_depth.expand_as(uvd[:, :, 2])  # B x M

    '''2. uvd->xyz'''''
    camparam = torch.cat((intr[:, 0:1, 0],intr[:, 1:2, 1],intr[:, 0:1, 2],intr[:, 1:2, 2]),1)
    camparam = camparam.unsqueeze(1).repeat(1, uvd.size(1), 1)  # B x M x 4
    xy = ((uv - camparam[:, :, 2:4]) / camparam[:, :, :2]) * \
        z.unsqueeze(2).expand_as(uv)  # B x M x 2
    return torch.cat((xy, z.unsqueeze(2)), -1)  # B x M x 3

def heatmap_to_keypoint2d(hm):
    b, c, w, h = hm.size()
    hm = hm.view(b,c,-1)
    hm = hm/torch.sum(hm,-1,keepdim=True)
    coord_map_x = torch.arange(0,w).view(-1,1).repeat(1,h).to(hm.device)
    coord_map_y = torch.arange(0,h).view(1,-1).repeat(w,1).to(hm.device)
    coord_map_x = coord_map_x.view(1,1,-1).float()
    coord_map_y = coord_map_y.view(1,1,-1).float()
    x = torch.sum(coord_map_x * hm,-1,keepdim=True)
    y = torch.sum(coord_map_y * hm,-1,keepdim=True)
    kp_2d = torch.cat((y,x),dim=-1)
    return kp_2d


def xyz_to_uvd( xyz, joint_root, joint_bone, intr=None, trans=None, scale=None, inp_res=256, mode='persp'):
    bs = xyz.shape[0]
    if mode in ['persp', 'perspective']:
        if intr is None:
            raise Exception("No intr found in perspective")
        z = xyz[:, :, 2]
        xy = xyz[:, :, :2]
        xy = xy / z.unsqueeze(-1).expand_as(xy)

        ''' 1. normalize depth : root_relative, scale_invariant '''
        root_depth = joint_root[:, -1].unsqueeze(-1)  # (B, 1)
        depth = (z - root_depth.expand_as(z)) / joint_bone.expand_as(z)

        '''2. xy->uv'''
        camparam = torch.zeros((bs, 4)).float().to(intr.device)  # (B, 4)
        camparam[:, 0] = intr[:, 0, 0]  # fx
        camparam[:, 1] = intr[:, 1, 1]  # fx
        camparam[:, 2] = intr[:, 0, 2]  # cx
        camparam[:, 3] = intr[:, 1, 2]  # cy
        camparam = camparam.unsqueeze(1).expand(-1, xyz.size(1), -1)  # B x M x 4
        uv = (xy * camparam[:, :, :2]) + camparam[:, :, 2:4]

        '''3. normalize uvd to 0~1'''
        uv = uv / inp_res
        depth = (depth - DEPTH_MIN) / DEPTH_RANGE

        return torch.cat((uv, depth.unsqueeze(-1)), -1)
    elif mode in ['ortho', 'orthogonal']:
        if trans is None or scale is None:
            raise Exception("No trans or scale found in orthorgnal")
        raise Exception("orth Unimplement !")
    else:
        raise Exception("Unkonwn proj type. should in ['persp', 'ortho']")
