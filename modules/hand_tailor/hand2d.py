import sys
sys.path.append('../../')

import os
import numpy as np
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
from PIL import Image
import matplotlib.pyplot as plt
import glob
from modules.hand_tailor.handataset import HandDataset
from backbones.bottleneck import BottleneckBlock
from modules.hand_tailor.network import *
from utils.loss import *
from helper import *
import cv2
import pytorch_lightning as pl
from torch.utils.data import DataLoader
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers import WandbLogger

class Hand2DNet(pl.LightningModule):
    def __init__(self):
        super().__init__()
        self.hand2d = Hand2D(nstacks=2, nblocks=1, njoints=21, block=BottleneckBlock)
        self.train_batch_size = 4
        self.val_batch_size = 4
        self.num_workers = 4
        self.learning_rate = 1e-4
        self.train_dataset = HandDataset( data_split='test', train=False, subset_name=['rhd'], data_root='./data',)
        self.val_dataset = HandDataset( data_split='test', train=False, subset_name=['rhd'], data_root='./data',)

    def forward(self, x):
        hm, enc = self.hand2d(x)
        return hm, enc

    def loss_func(self, data, predictions):
        hm_veil = data['heatmap_veil']
        hm_veil = hm_veil.unsqueeze(-1)
        batch_size = hm_veil.size(0)
        hm_loss = torch.tensor([0]).to(self.device)
        for pred_hm in predictions['heatmap']:
            njoints = pred_hm.size(1)
            pred_hm = pred_hm.reshape((batch_size, njoints, -1)).split(1, 1)
            targ_hm = data['heatmap'].reshape((batch_size, njoints, -1)).split(1, 1)
            for idx in range(njoints):
                pred_hmi = pred_hm[idx].squeeze()  # (b, 1, 4096)->(b, 4096)
                targ_hmi = targ_hm[idx].squeeze()
                hm_loss += 0.5 * torch.nn.functional.mse_loss(
                    pred_hmi.mul(hm_veil[:, idx]),  # (b, 4096) mul (b, 1)
                    targ_hmi.mul(hm_veil[:, idx])
                )
        return {'kp2d_loss': hm_loss}

    def training_step(self, batch, batch_idx):
        model_inputs, data = batch
        hm, enc = self.hand2d(model_inputs['image'])
        predictions = {'heatmap': hm}
        losses = self.loss_func(data, predictions)
        for name, loss in losses.items():
            self.log(name, loss)
        return losses['kp2d_loss']

    def validation_step(self, batch, batch_idx):
        model_inputs, data = batch
        hm, enc = self.hand2d(model_inputs['image'])
        predictions = {'heatmap': hm}
        losses = self.loss_func(data, predictions)
        for name, loss in losses.items():
            self.log(name, loss)
        return losses

    def test_step(self, batch, batch_idx):
        model_inputs, data = batch
        hm, enc = self.hand2d(model_inputs['image'])
        kp_2d = heatmap_to_keypoint2d(data['heatmap']) * 4
        kp_2d_pred = heatmap_to_keypoint2d(hm[-1]) * 4
        image = data['image']
        image = (image +1) * 127.5
        image = image.permute(0, 2, 3, 1)
        image = image.numpy().astype(np.uint8)
        image = np.ascontiguousarray(image, dtype=np.uint8)
        for idx in range(image.shape[0]):
            for j in range(21):
                xpt = int(kp_2d[idx][j][0]); ypt =  int(kp_2d[idx][j][1])
                cv2.circle(image[idx], (xpt, ypt), 1, (0, 255, 0), 2)
                xpt = int(kp_2d_pred[idx][j][0]); ypt =  int(kp_2d_pred[idx][j][1])
                cv2.circle(image[idx], (xpt, ypt), 1, (255, 0, 0), 2)
            im = Image.fromarray(image[idx])
            im.save(f'test_{idx}.png')

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

    def train_dataloader(self):
        return DataLoader(self.train_dataset, batch_size=self.train_batch_size, num_workers = self.num_workers)

    def val_dataloader(self):
        return DataLoader(self.val_dataset, batch_size=self.val_batch_size, num_workers = self.num_workers)

    def test_dataloader(self):
        return DataLoader(self.val_dataset, batch_size=self.val_batch_size, num_workers = self.num_workers)

if __name__ == '__main__':
    model = Hand2DNet()
#    checkpoint_callback = ModelCheckpoint(monitor='kp2d_loss_val',
#                                          dirpath='./weights/',
#                                          filename='hand2d-{epoch:02d}-{val_loss:.4f}',
#                                          save_top_k=3,
#                                          mode='min')
#    wandb_logger = WandbLogger(log_model = True)
#    trainer = pl.Trainer( max_epochs = 50, callbacks=[checkpoint_callback], logger=None, fast_dev_run  = True)
    trainer = pl.Trainer(fast_dev_run = True)
#    model2 = model.load_from_checkpoint('/Users/coreqode/Downloads/test42.ckpt')
#    torch.save(model2.state_dict(), './weights/hand2d_model.pt')
    model.load_state_dict(torch.load('./weights/hand2d_model.pt'))
    trainer.test(model)
    #trainer.fit(model)
