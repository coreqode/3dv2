import sys
sys.path.append('../../')

from utils.utils import hm_to_uvd, uvd2xyz
from modules.hand_tailor.network import Hand2Dto3D, Hand2D, IKNet
from backbones.resnet18 import ResNet18
from backbones.bottleneck import BottleneckBlock
from base.base_module import BaseModule
from dataset import HandDataset
import torch.nn as nn
import torch
from torchsummary import summary
import numpy as np

class HandTailor(BaseModule):
    def __init__(self):
        super(HandTailor).__init__()
        self.epoch = 100
        self.njoints = 21
        self.data_dir = "/home/coreqode/projects/3d_computer_vision/datasets/RHD/"

    def define_dataset(self):
        self.train_dataset = HandDataset(
            data_split='test', data_root='../../data/')
        self.val_dataset = HandDataset(
            data_split='test', data_root='../../data/')

    def define_model(self):
        class HandNet(nn.Module):
            def __init__(
                self,
                njoints=21,
            ):
                super(HandNet, self).__init__()
                self.njoints = njoints
                self.decoder = ResNet18()

                hidden_size = [512, 512, 1024, 1024, 512, 256]
                in_neurons = 512
                out_neurons = 12
                neurons = [in_neurons] + hidden_size

                self.hand2d = Hand2D(nstacks=2, nblocks=1,
                                     njoints=self.njoints, block=BottleneckBlock)
                
                self.hand2dto3d = Hand2Dto3D(
                    nstacks=2, nblocks=1, njoints=self.njoints, block=BottleneckBlock)

                shapereg_layers = []
                for layer_idx, (inps, outs) in enumerate(zip(neurons[:-1], neurons[1:])):
                    shapereg_layers.append(nn.Linear(inps, outs))
                    shapereg_layers.append(nn.BatchNorm1d(outs))
                    shapereg_layers.append(nn.ReLU())

                shapereg_layers.append(nn.Linear(neurons[-1], out_neurons))
                self.shapereg_layers = nn.Sequential(*shapereg_layers)
                self.sigmoid = nn.Sigmoid()
                self.iknet = IKNet()

                self.ref_bone_link = (0, 9)
                self.joint_root_idx = 9

            def forward(self, model_inputs):
                x = model_inputs['image']
                intr = model_inputs['camera_parameters']
                hm, enc = self.hand2d(x)
                hm3d, enc3d = self.hand2dto3d(enc[-1])
                uvd = []
                uvd.append(hm_to_uvd(hm3d[-1]))
                hm.append(hm3d[-1][:, :21, ...])
                feat = self.decoder(enc3d[-1])
                shape_vector = self.shapereg_layers(feat)
                bone = self.sigmoid(shape_vector[:, 0:1])
                root = self.sigmoid(shape_vector[:, 1:2])
                beta = shape_vector[:, 2:]
                joint = uvd2xyz(uvd[-1], root, bone, intr=intr, mode='persp')
                joint_root = joint[:, self.joint_root_idx, :].unsqueeze(1)
                joint_ = joint - joint_root
                bone_pred = torch.zeros((x.shape[0], 1)).to(x.device)
                for jid, nextjid in zip(self.ref_bone_link[:-1], self.ref_bone_link[1:]):
                    bone_pred += torch.norm(
                        joint_[:, jid, :] - joint_[:, nextjid, :],
                        dim=1, keepdim=True
                    )
                bone_pred = bone_pred.unsqueeze(1)  # (B,1,1)
                bone_vis = bone_pred
                _joint_ = joint_ / bone_pred

                so3, quat = self.iknet(joint)
                out = {'heatmap': hm[-1], 'so3': so3, 'beta': beta,
                       'joint_root': joint_root, 'bone_vis': bone_vis}
                return out
        self.model = HandNet()

    def define_loss_meter(self, losses):
        pass

def main():
    ht = HandTailor()
    ht.define_model()
    model_inputs, data = ht._parse()
    out = ht.model(model_inputs)
    print(out.keys())
    # ht.init()
    # ht.train()


if __name__ == "__main__":
    main()
