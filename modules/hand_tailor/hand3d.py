import sys

sys.path.append("../../")

import os
import numpy as np
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
from PIL import Image
import matplotlib.pyplot as plt
import glob
from modules.hand_tailor.handataset import HandDataset
from backbones.bottleneck import BottleneckBlock
from modules.hand_tailor.network import *
from utils.loss import *
from helper import *
import cv2
import pytorch_lightning as pl
from torch.utils.data import DataLoader
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers import WandbLogger
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from hand2d import Hand2DNet
from pytorch_lightning.utilities import rank_zero_info

class BottleneckBlock_(nn.Module):
    def __init__(self, in_planes, out_planes):
        super(BottleneckBlock_, self).__init__()

        self.in_planes = in_planes
        self.out_planes = out_planes

        mid_planes = (out_planes // 2 ) if out_planes >= in_planes else in_planes // 2
        self.bn1 = nn.BatchNorm2d(in_planes)
        self.conv1 = nn.Conv2d(in_planes, mid_planes, kernel_size=1, bias=True)

        self.bn2 = nn.BatchNorm2d(mid_planes)
        self.conv2 = nn.Conv2d(mid_planes, mid_planes, kernel_size=3, stride=1, padding=1, bias=True)

        self.bn3 = nn.BatchNorm2d(mid_planes)
        self.conv3 = nn.Conv2d(mid_planes, out_planes, kernel_size=1, bias=True)
        self.relu = nn.ReLU(inplace=True)

        if in_planes != out_planes:
            self.conv4 = nn.Conv2d(in_planes, out_planes, bias=True, kernel_size=1)


    def forward(self, x):
        residual = x

        out = self.bn1(x)
        out = self.relu(out)
        out = self.conv1(out)

        out = self.bn2(out)
        out = self.relu(out)
        out = self.conv2(out)

        out = self.bn3(out)
        out = self.relu(out)
        out = self.conv3(out)

        if self.in_planes != self.out_planes:
            residual = self.conv4(x)

        out += residual
        return out

def clean_state_dict(state_dict):
    """save a cleaned version of model without dict and DataParallel
    Arguments:
        state_dict {collections.OrderedDict} -- [description]
    Returns:
        clean_model {collections.OrderedDict} -- [description]
    """

    clean_model = state_dict
    # create new OrderedDict that does not contain `module.`
    from collections import OrderedDict
    clean_model = OrderedDict()
    if any(key.startswith('module') for key in state_dict):
        for k, v in state_dict.items():
            name = k[7:]  # remove `module.`
            clean_model[name] = v
    else:
        return state_dict

    return clean_model

def uvd2xyz(
        uvd,
        joint_root,
        joint_bone,
        intr=None,
        trans=None,
        scale=None,
        inp_res=256,
        mode='persp'
):
    bs = uvd.shape[0]
    if mode in ['persp', 'perspective']:
        if intr is None:
            raise Exception("No intr found in perspective")
        '''1. denormalized uvd'''
        uv = uvd[:, :, :2] * inp_res  # 0~256
        depth = (uvd[:, :, 2] * 3.5) -1.5
        root_depth = joint_root[:, -1].unsqueeze(-1)  # (B, 1)
        z = depth * joint_bone.expand_as(uvd[:, :, 2]) + \
            root_depth.expand_as(uvd[:, :, 2])  # B x M

        '''2. uvd->xyz'''
        camparam = torch.zeros((bs, 4)).float().to(intr.device)  # (B, 4)
        camparam[:, 0] = intr[:, 0, 0]  # fx
        camparam[:, 1] = intr[:, 1, 1]  # fx
        camparam[:, 2] = intr[:, 0, 2]  # cx
        camparam[:, 3] = intr[:, 1, 2]  # cy
        camparam = camparam.unsqueeze(1).expand(-1, uvd.size(1), -1)  # B x M x 4
        xy = ((uv - camparam[:, :, 2:4]) / camparam[:, :, :2]) * \
             z.unsqueeze(-1).expand_as(uv)  # B x M x 2
        return torch.cat((xy, z.unsqueeze(-1)), -1)  # B x M x 3
    elif mode in ['ortho', 'orthogonal']:
        if trans is None or scale is None:
            raise Exception("No trans or scale found in orthorgnal")
        raise Exception("orth Unimplement !")
    else:
        raise Exception("Unkonwn mode type. should in ['persp', 'ortho']")

def integral_pose(hm3d):
    d_accu = torch.sum(hm3d, dim=[3, 4])
    v_accu = torch.sum(hm3d, dim=[2, 4])
    u_accu = torch.sum(hm3d, dim=[2, 3])

    weightd = torch.arange(d_accu.shape[-1], dtype=d_accu.dtype, device=d_accu.device) / d_accu.shape[-1]
    weightv = torch.arange(v_accu.shape[-1], dtype=v_accu.dtype, device=v_accu.device) / v_accu.shape[-1]
    weightu = torch.arange(u_accu.shape[-1], dtype=u_accu.dtype, device=u_accu.device) / u_accu.shape[-1]

    d_ = d_accu.mul(weightd)
    v_ = v_accu.mul(weightv)
    u_ = u_accu.mul(weightu)

    d_ = torch.sum(d_, dim=-1, keepdim=True)
    v_ = torch.sum(v_, dim=-1, keepdim=True)
    u_ = torch.sum(u_, dim=-1, keepdim=True)

    uvd = torch.cat([u_, v_, d_], dim=-1)
    return uvd

class Hand3DNet(pl.LightningModule):
    def __init__(self, train_batch_size = 1, val_batch_size = 1, num_workers = 4, learning_rate = 1e-4):
        super().__init__()
        self.train_batch_size = train_batch_size
        self.val_batch_size = val_batch_size
        self.num_workers = num_workers
        self.learning_rate = learning_rate
        self.train_dataset = HandDataset(
            data_split="test",
            train=True,
            subset_name=["rhd"],
            data_root="./data",
        )
        self.val_dataset = HandDataset(
            data_split="test",
            train=False,
            subset_name=["rhd"],
            data_root="./data",
        )

        # hand2d = Hand2D(nstacks=2, nblocks=1, njoints=21, block=BottleneckBlock)
        model = SeedNet(  nstacks=2, nblocks=1, njoints=21, block=BottleneckBlock_,)
        checkpoint = torch.load('checkpoints/ckp_seednet_all.pth.tar', map_location = torch.device('cpu'))

        pretrain_dict = clean_state_dict(checkpoint['state_dict'])
        model_state = model.state_dict()
        state = {}
        for k, v in pretrain_dict.items():
            if k in model_state:
                state[k] = v
            else:
                print(k, ' is NOT in current model')
        model_state.update(state)
        model.load_state_dict(model_state)
        self.hand2d = model
        #pretrained_dict = torch.load('./weights/hand2d_model.pt')
        # self.hand2d = self.parse_state_dict(hand2d, pretrained_dict )
        # for param in self.hand2d.parameters():
        #         param.requires_grad = False
        # self.hand2dto3d = Hand2Dto3D(
        #     nstacks=2, nblocks=1, njoints=21, block=BottleneckBlock
        # )
        self.hand2dto3d = LiftNet(nstacks=2, nblocks=1, njoints=21, block=BottleneckBlock_)
#        hidden_size=[512, 512, 1024, 1024, 512, 256]
#        in_neurons = 512
#        out_neurons = 12
#        neurons = [in_neurons] + hidden_size
#        shapereg_layers = []
#        for layer_idx, (inps, outs) in enumerate(zip(neurons[:-1], neurons[1:])):
#            shapereg_layers.append(nn.Linear(inps, outs))
#            shapereg_layers.append(nn.BatchNorm1d(outs))
#            shapereg_layers.append(nn.ReLU())
#
#        shapereg_layers.append(nn.Linear(neurons[-1], out_neurons))
#        self.shapereg_layers = nn.Sequential(*shapereg_layers)
#        self.decoder = ResNet18()
#        self.sigmoid = nn.Sigmoid()

        self.ref_bone_link = (0, 9)
        self.joint_root_idx = 9

    def parse_state_dict(self, model, pretrained_dict, lightning = True):
        model_dict = model.state_dict()
        new_state = {}
        for k, v in pretrained_dict.items():
            if lightning:
                temp = '.'.join(k.split('.')[1:])
            else:
                temp= k
            if temp in model_dict:
                new_state[temp] = v
        model_dict.update(new_state)
        model.load_state_dict(model_dict)
        return model

    def forward(self, x, intr):
        hm, dep, enc = self.hand2d(x)
        hm3d, enc3d = self.hand2dto3d(hm[-1], enc[-1])

        predictions = {
            "heatmap_2d": hm,
            "heatmap_3d": hm3d,
            "encoding_3d": enc3d
        }
        return predictions


    def loss_func(self, data, predictions):

        # joint loss
        hm3d = predictions['heatmap_3d']
        intr = data['intr']
        joint_root = data['joint_root']
        joint_bone = data['joint_bone']
        gt_joint = data["joint"]

        joint_loss = torch.tensor([0]).float().to(self.device)
        for i in range(len(hm3d)):
#            uvd = voxel_to_uvd(hm3d[i])
            uvd = integral_pose(hm3d[i])
            joint = uvd2xyz(uvd, joint_root, joint_bone, intr, mode="persp")
            j_loss = torch.nn.functional.mse_loss( joint * 1000, gt_joint* 1000)
            joint_loss += j_loss

#        hm_veil = data["heatmap_veil"]
#        hm_veil = hm_veil.unsqueeze(-1)
#        batch_size = hm_veil.size(0)
#        hm_loss = torch.tensor([0]).float().to(self.device)
#
#        for i in range(len(hm3d)):
#            pred_hm = hm3d[i][:, :21]
#            njoints = pred_hm.size(1)
#            pred_hm = pred_hm.reshape((batch_size, njoints, -1)).split(1, 1)
#            targ_hm = data["heatmap"].reshape((batch_size, njoints, -1)).split(1, 1)
#            for idx in range(njoints):
#                pred_hmi = pred_hm[idx].squeeze()  # (b, 1, 4096)->(b, 4096)
#                targ_hmi = targ_hm[idx].squeeze()
#                hm_loss += 0.5 * torch.nn.functional.mse_loss(
#                    pred_hmi.mul(hm_veil[:, idx]),  # (b, 4096) mul (b, 1)
#                    targ_hmi.mul(hm_veil[:, idx]),
#
#                )
#        final_loss = 100*joint_loss + hm_loss
        return {
            "joint_loss": joint_loss,
#            "hm_los": hm_loss,
            "final_loss": joint_loss
        }

    def training_step(self, batch, batch_idx):
        model_inputs, data = batch
        image = model_inputs["image"]
        intr = model_inputs["intr"]
        predictions = self(image, intr)
        losses = self.loss_func(data, predictions)

        for name, loss in losses.items():
            self.log(name, loss, prog_bar = True, on_step = True)
        return {'loss' : losses['final_loss'],
                'progress_bar':losses}


    def validation_step(self, batch, batch_idx):
        losses = self.training_step(batch, batch_idx)['progress_bar']
        for name, loss in losses.items():
            self.log(f"{name}_val", loss, prog_bar = True)
        return {'loss': losses['final_loss'],
                'progress_bar': losses        }

    def test_step(self, batch, batch_idx):
        model_inputs, data = batch
        intr = data['intr']
        image = model_inputs['image']
        predictions = self(image, intr)
        #hm3d = predictions['heatmap_3d']
        hm3d = predictions['heatmap_2d']
        print(hm3d[-1].shape)
        joint_root = data['joint_root']
        joint_bone = data['joint_bone']
        gt_joint = data["joint"]

#        for i in range(len(hm3d)):
#            uvd = voxel_to_uvd(hm3d[i])
#            joint = uvd_to_xyz(uvd, joint_root, joint_bone, intr, mode="persp")
#        print(gt_joint[0])
#        print(joint[0])

        kp_2d = heatmap_to_keypoint2d(data["heatmap"]) * 4
        kp_2d_pred = heatmap_to_keypoint2d(hm3d[-1]) * 4
        #kp_2d_pred = uvd * 64 *4

        image = data["image"]
        image = (image + 1) * 127.5
        image = image.permute(0, 2, 3, 1)
        image = image.numpy().astype(np.uint8)
        image = np.ascontiguousarray(image, dtype=np.uint8)
        for idx in range(image.shape[0]):
            for j in range(21):
                xpt = int(kp_2d[idx][j][0])
                ypt = int(kp_2d[idx][j][1])
                cv2.circle(image[idx], (xpt, ypt), 1, (0, 255, 0), 2)
                xpt = int(kp_2d_pred[idx][j][0])
                ypt = int(kp_2d_pred[idx][j][1])
                cv2.circle(image[idx], (xpt, ypt), 1, (255, 0, 0), 2)
            im = Image.fromarray(image[idx])
            im.save(f"test_{idx}.png")

    def configure_optimizers(self):
        parameters = list(self.parameters())
        trainable_parameters = list(filter(lambda p: p.requires_grad, parameters))
        rank_zero_info(
            f"The model will start training with only {len(trainable_parameters)} "
            f"trainable parameters out of {len(parameters)}."
        )
        optimizer = torch.optim.Adam(trainable_parameters, lr=self.learning_rate)
        return optimizer

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset,
            batch_size=self.train_batch_size,
            num_workers=self.num_workers,
            pin_memory = True
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_dataset,
            batch_size=self.val_batch_size,
            num_workers=self.num_workers,
            pin_memory = True
        )

    def test_dataloader(self):
        return DataLoader(
            self.val_dataset,
            batch_size=self.val_batch_size,
            num_workers=self.num_workers,
            pin_memory = True
        )


if __name__ == "__main__":
    checkpoint_callback = ModelCheckpoint(
        monitor="final_loss_val",
        dirpath="./weights/",
        filename="Hand2D-{epoch:02d}-{val_loss:.6f}",
        save_top_k=3,
        mode="min",
    )

    early_stop_callback = EarlyStopping(
        monitor="final_loss_val", min_delta=0.00, patience=3, verbose=True, mode="min"
    )

    wandb_logger = WandbLogger(log_model=True, name='hand3d', project='handtailor')
#    trainer = pl.Trainer(
#        max_epochs=50, callbacks=[checkpoint_callback, early_stop_callback], logger=None, fast_dev_run=True, gpus = 0
#    )
    trainer = pl.Trainer(fast_dev_run = True)

    model = Hand3DNet(train_batch_size = 4, val_batch_size = 4, num_workers = 4, learning_rate = 1e-4)
    #model2 = model.load_from_checkpoint('/Users/coreqode/Downloads/hand3d_4.ckpt')
    trainer.test(model)
    #trainer.fit(model)
