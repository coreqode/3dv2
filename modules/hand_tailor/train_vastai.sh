apt-get install sudo --assume-yes
apt-get install ffmpeg libsm6 libxext6  -y --assume-yes
apt-get update
apt-get install unzip --assume-yes
apt-get install vim --assume-yes
apt-get install wget --assume-yes
pip install opencv-python
pip install termcolor
pip install transforms3d matplotlib wandb progress
pip install torchsummary
pip install torchvision
pip install gdown
pip install pytorch-lightning

mkdir data
cd data
mkdir RHD

## data
#wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate "https://docs.google.com/uc?export=download&id=1EhOEbr_CcmUVzE3AHssGgAm3ZVhe8IVO" -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1EhOEbr_CcmUVzE3AHssGgAm3ZVhe8IVO" -O sik1m.zip && rm -rf /tmp/cookies.txt
#
wget https://lmb.informatik.uni-freiburg.de/data/RenderedHandpose/RHD_v1-1.zip
unzip RHD_v1-1.zip
mv RHD_published_v2/ RHD/

cd ..
mkdir weights
cd weights
gdown https://drive.google.com/uc?id=1exbc39d5WYLsbGUFHXy011rnFJcHhgzd
