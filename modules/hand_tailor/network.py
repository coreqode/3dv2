import sys
sys.path.append('../../')

import torch
import torch.nn as nn
import torch.nn.functional as F

from utils.util import *
from backbones.bottleneck import BottleneckBlock
from backbones.hourglass import Hourglass

class HourglassBisected(nn.Module):
    def __init__(
            self,
            block,
            nblocks,
            in_planes,
            depth=4
    ):
        super(HourglassBisected, self).__init__()
        self.depth = depth
        self.hg = self._make_hourglass(block, nblocks, in_planes, depth)

    def _make_hourglass(self, block, nblocks, in_planes, depth):
        hg = []
        for i in range(depth):
            res = []
            for j in range(3):
                _res = []
                if j == 1:
                    _res.append(self._make_residual(block, nblocks, in_planes))
                else:
                    _res.append(self._make_residual(block, nblocks, in_planes))
                    _res.append(self._make_residual(block, nblocks, in_planes))

                res.append(nn.ModuleList(_res))

            if i == 0:
                _res = []
                _res.append(self._make_residual(block, nblocks, in_planes))
                _res.append(self._make_residual(block, nblocks, in_planes))
                res.append(nn.ModuleList(_res))

            hg.append(nn.ModuleList(res))
        return nn.ModuleList(hg)

    def _make_residual(self, block, nblocks, in_planes):
        layers = []
        for i in range(0, nblocks):
            layers.append(block(in_planes, in_planes))
        return nn.Sequential(*layers)

    def _hourglass_foward(self, n, x):
        up1_1 = self.hg[n - 1][0][0](x)
        up1_2 = self.hg[n - 1][0][1](x)
        low1 = F.max_pool2d(x, 2, stride=2)
        low1 = self.hg[n - 1][1][0](low1)

        if n > 1:
            low2_1, low2_2, latent = self._hourglass_foward(n - 1, low1)
        else:
            latent = low1
            low2_1 = self.hg[n - 1][3][0](low1)
            low2_2 = self.hg[n - 1][3][1](low1)

        low3_1 = self.hg[n - 1][2][0](low2_1)
        low3_2 = self.hg[n - 1][2][1](low2_2)

        up2_1 = F.interpolate(low3_1, scale_factor=2)
        up2_2 = F.interpolate(low3_2, scale_factor=2)
        out_1 = up1_1 + up2_1
        out_2 = up1_2 + up2_2

        return out_1, out_2, latent

    def forward(self, x):
        return self._hourglass_foward(self.depth, x)
class IKNet(nn.Module):
    def __init__(
        self,
        njoints=21,
        hidden_size_pose=[256, 512, 1024, 1024, 512, 256],
    ):
        super(IKNet, self).__init__()
        self.njoints = njoints
        in_neurons = 3 * njoints
        out_neurons = 16 * 4 # 16 quats
        neurons = [in_neurons] + hidden_size_pose

        invk_layers = []
        for layer_idx, (inps, outs) in enumerate(zip(neurons[:-1], neurons[1:])):
            invk_layers.append(nn.Linear(inps, outs))
            invk_layers.append(nn.BatchNorm1d(outs))
            invk_layers.append(nn.ReLU())
        invk_layers.append(nn.Linear(neurons[-1], out_neurons))
        self.invk_layers = nn.Sequential(*invk_layers)

    def forward(self, joint):
        joint = joint.contiguous().view(-1, self.njoints*3)
        quat = self.invk_layers(joint)
        quat = quat.view(-1, 16, 4)
        quat = normalize_quaternion(quat)
        so3 = quaternion_to_angle_axis(quat).contiguous()
        so3 = so3.view(-1, 16 * 3)
        return so3, quat

class ResNet18(nn.Module):
    def __init__(self, block=BottleneckBlock, out_plane=256):
        super(ResNet18, self).__init__()
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_residual(block, 2, 256, 512)
        self.layer2 = self._make_residual(block, 2, 512, 512)
        self.layer3 = self._make_residual(block, 2, 512, 512)
        self.layer4 = self._make_residual(block, 2, 512, 512)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def _make_residual(self, block, nblocks, in_planes, out_planes):
        layers = []
        layers.append(block(in_planes, out_planes))
        self.in_planes = out_planes
        for i in range(1, nblocks):
            layers.append(block(self.in_planes, out_planes))
        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.maxpool(x)#32
        x = self.layer1(x)
        x = self.maxpool(x)#16
        x = self.layer2(x)
        x = self.maxpool(x)#8
        x = self.layer3(x)
        x = self.maxpool(x)#4
        x = self.layer4(x)
        x = self.avgpool(x)#1
        x = torch.flatten(x,1)

        return x

class Hand2D(nn.Module):
    def __init__(
        self,
        nstacks=2,
        nblocks=1,
        njoints=21,
        block=BottleneckBlock,
    ):
        super(Hand2D, self).__init__()
        self.njoints  = njoints
        self.nstacks  = nstacks
        self.in_planes = 64

        self.conv1 = nn.Conv2d(3, self.in_planes, kernel_size=7, stride=2, padding=3, bias=True)
        self.bn1 = nn.BatchNorm2d(self.in_planes)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(2, stride=2)
        self.sigmoid = nn.Sigmoid()

        self.layer1 = self._make_residual(block, nblocks, self.in_planes, 2*self.in_planes)
        self.layer2 = self._make_residual(block, nblocks, self.in_planes, 2*self.in_planes)
        self.layer3 = self._make_residual(block, nblocks, self.in_planes, self.in_planes)

        ch = self.in_planes

        hg2b, res, fc, hm = [],[],[],[]
        for i in range(nstacks):
            hg2b.append(Hourglass(block, nblocks, ch, depth=4))
            res.append(self._make_residual(block, nblocks, ch, ch))
            hm.append(nn.Conv2d(ch, njoints, kernel_size=1, bias=True))
            fc.append(self._make_fc(ch + njoints, ch))

        self.hg2b  = nn.ModuleList(hg2b)
        self.res  = nn.ModuleList(res)
        self.fc   = nn.ModuleList(fc)
        self.hm   = nn.ModuleList(hm)

    def _make_fc(self, in_planes, out_planes):
        conv = nn.Conv2d(in_planes, out_planes, kernel_size=1, bias=False)
        bn = nn.BatchNorm2d(out_planes)
        return nn.Sequential(conv, bn, self.relu)

    def _make_residual(self, block, nblocks, in_planes, out_planes):
        layers = []
        layers.append(block(in_planes, out_planes) )
        self.in_planes = out_planes
        for i in range(1, nblocks):
            layers.append(block(self.in_planes, out_planes))
        return nn.Sequential(*layers)

    def forward(self, x):

        l_est_hm, l_enc = [], []
        net = self.conv1(x)
        net = self.bn1(net)
        net = self.relu(net)

        net = self.layer1(net)
        net = self.maxpool(net)
        net = self.layer2(net)
        net = self.layer3(net)

        for i in range(self.nstacks):
            net = self.hg2b[i](net)
            net = self.res[i](net)
            est_hm = self.sigmoid(self.hm[i](net))
            net = torch.cat((net,est_hm),1)
            net = self.fc[i](net)
            l_est_hm.append(est_hm)
            l_enc.append(net)
        assert len(l_est_hm) == self.nstacks
        return l_est_hm, l_enc

class Hand2Dto3D(nn.Module):
    def __init__(
        self,
        nstacks=2,
        nblocks=1,
        njoints=21,
        block=BottleneckBlock,
    ):
        super(Hand2Dto3D, self).__init__()
        self.njoints = njoints
        self.nstacks = nstacks
        self.in_planes = 256

        self.relu = nn.ReLU(inplace=True)
        self.sigmoid = nn.Sigmoid()

        ch = self.in_planes

        hg3d2b, res, fc, _fc = [], [], [], []
        hm3d, _hm3d = [], []
        for i in range(nstacks):
            hg3d2b.append(Hourglass(block, nblocks, ch, depth=4))
            res.append(self._make_residual(block, nblocks, ch, ch))
            fc.append(self._make_fc(ch + 2*njoints, ch))
            hm3d.append(nn.Conv2d(ch, 2*njoints, kernel_size=1, bias=True))

        self._hm_prev = nn.Conv2d(21, self.in_planes, kernel_size=1, bias=True)
        self.hg3d2b = nn.ModuleList(hg3d2b)
        self.res = nn.ModuleList(res)
        self.fc = nn.ModuleList(fc)
        self.hm3d = nn.ModuleList(hm3d)

    def _make_fc(self, in_planes, out_planes):
        conv = nn.Conv2d(in_planes, out_planes, kernel_size=1, bias=False)
        bn = nn.BatchNorm2d(out_planes)
        return nn.Sequential(conv, bn, self.relu)

    def _make_residual(self, block, nblocks, in_planes, out_planes):
        layers = []
        layers.append(block(in_planes, out_planes))
        self.in_planes = out_planes
        for i in range(1, nblocks):
            layers.append(block(self.in_planes, out_planes))
        return nn.Sequential(*layers)

    def forward(self, hm, enc):
        l_est_hm3d, l_enc3d = [], []
        net = self._hm_prev(hm) + enc

        for i in range(self.nstacks):
            net = self.hg3d2b[i](net)
            net = self.res[i](net)
            hm3d = self.sigmoid(self.hm3d[i](net))
            net = torch.cat((net, hm3d), 1)
            net = self.fc[i](net)
            l_est_hm3d.append(hm3d)
            l_enc3d.append(net)

        return l_est_hm3d, l_enc3d

class LiftNet(nn.Module):
    def __init__(
            self,
            nstacks=2,
            nblocks=1,
            njoints=21,
            block=BottleneckBlock,
    ):
        super(LiftNet, self).__init__()
        self.njoints = njoints
        self.nstacks = nstacks
        self.relu = nn.ReLU(inplace=True)
        self.in_planes = 256
        ch = self.in_planes
        z_res = [32, 64]

        # encoding previous hm and mask
        self._hm_prev = nn.Conv2d(njoints, self.in_planes, kernel_size=1, bias=True)
        self._mask_prev = nn.Conv2d(1, self.in_planes, kernel_size=1, bias=True)

        hg3d2b, res1, res2, fc1, _fc1, fc2, _fc2 = [], [], [], [], [], [], []
        hm3d, _hm3d, dep, _dep = [], [], [], []
        for i in range(nstacks):
            hg3d2b.append(HourglassBisected(block, nblocks, ch, depth=4))
            res1.append(self._make_residual(block, nblocks, ch, ch))
            res2.append(self._make_residual(block, nblocks, ch, ch))
            fc1.append(self._make_fc(ch, ch))
            fc2.append(self._make_fc(ch, ch))

            hm3d.append(
                nn.Sequential(
                    nn.Conv2d(ch, z_res[i] * njoints, kernel_size=1, bias=True),
                    self.relu,
                )
            )
            dep.append(
                nn.Sequential(
                    nn.Conv2d(ch, 1, kernel_size=1, bias=True),
                    self.relu,
                )
            )

            if i < nstacks - 1:
                _fc1.append(nn.Conv2d(ch, ch, kernel_size=1, bias=False))
                _fc2.append(nn.Conv2d(ch, ch, kernel_size=1, bias=False))
                _hm3d.append(nn.Conv2d(z_res[i] * njoints, ch, kernel_size=1, bias=False))
                _dep.append(nn.Conv2d(1, ch, kernel_size=1, bias=False))

        self.z_res = z_res
        self.hg3d2b = nn.ModuleList(hg3d2b)  # hgs: hourglass stack
        self.res1 = nn.ModuleList(res1)
        self.fc1 = nn.ModuleList(fc1)
        self._fc1 = nn.ModuleList(_fc1)
        self.res2 = nn.ModuleList(res2)
        self.fc2 = nn.ModuleList(fc2)
        self._fc2 = nn.ModuleList(_fc2)
        self.hm3d = nn.ModuleList(hm3d)
        self._hm3d = nn.ModuleList(_hm3d)
        self.dep = nn.ModuleList(dep)
        self._dep = nn.ModuleList(_dep)

    def _make_fc(self, in_planes, out_planes):
        bn = nn.BatchNorm2d(in_planes)
        conv = nn.Conv2d(in_planes, out_planes, kernel_size=1, bias=False)
        return nn.Sequential(conv, bn, self.relu)

    def _make_residual(self, block, nblocks, in_planes, out_planes):
        layers = []
        layers.append(block(in_planes, out_planes))
        self.in_planes = out_planes
        for i in range(1, nblocks):
            layers.append(block(self.in_planes, out_planes))
        return nn.Sequential(*layers)

    def forward(self, est_hm, enc):
        x = self._hm_prev(est_hm) + enc

        l_hm3d, l_dep, l_latent = [], [], []

        for i in range(self.nstacks):
            y_1, y_2, latent = self.hg3d2b[i](x)

            l_latent.append(latent)

            y_1 = self.res1[i](y_1)
            y_1 = self.fc1[i](y_1)
            hm3d = self.hm3d[i](y_1)
            hm3d_out = hm3d.view(
                hm3d.shape[0],  # B
                self.njoints,  # 21
                self.z_res[i],  # z_res (32, 64)
                hm3d.shape[-2],  # H=64
                hm3d.shape[-1]  # W=64
            )
            hm3d_out = hm3d_out / (
                    torch.sum(hm3d_out, dim=[2, 3, 4], keepdim=True) + 1e-6
            )
            l_hm3d.append(hm3d_out)

            y_2 = self.res2[i](y_2)
            y_2 = self.fc2[i](y_2)
            est_dep = self.dep[i](y_2)
            l_dep.append(est_dep)

            if i < self.nstacks - 1:
                _fc1 = self._fc1[i](y_1)
                _hm3d = self._hm3d[i](hm3d)

                _fc2 = self._fc2[i](y_2)
                _dep = self._dep[i](est_dep)
                x = x + _fc1 + _fc2 + _hm3d + _dep

        return l_hm3d,  l_latent[-1]


class SeedNet(nn.Module):
    def __init__(
        self,
        nstacks=2,
        nblocks=1,
        njoints=21,
        block=BottleneckBlock,
    ):
        super(SeedNet, self).__init__()
        self.njoints  = njoints
        self.nstacks  = nstacks
        self.in_planes = 64

        self.conv1 = nn.Conv2d(3, self.in_planes, kernel_size=7, stride=2, padding=3, bias=True)
        self.bn1 = nn.BatchNorm2d(self.in_planes)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(2, stride=2)
        self.layer1 = self._make_residual(block, nblocks, self.in_planes, 2*self.in_planes)
        # current self.in_planes is 64 * 2 = 128
        self.layer2 = self._make_residual(block, nblocks, self.in_planes, 2*self.in_planes)
        # current self.in_planes is 128 * 2 = 256
        self.layer3 = self._make_residual(block, nblocks, self.in_planes, self.in_planes)

        ch = self.in_planes # 256

        hg2b, res1, res2, fc1, _fc1, fc2, _fc2= [],[],[],[],[],[],[]
        hm, _hm, mask, _mask = [], [], [], []
        for i in range(nstacks): # 2
            hg2b.append(HourglassBisected(block, nblocks, ch, depth=4))
            res1.append(self._make_residual(block, nblocks, ch, ch))
            res2.append(self._make_residual(block, nblocks, ch, ch))
            fc1.append(self._make_fc(ch, ch))
            fc2.append(self._make_fc(ch, ch))
            hm.append(nn.Conv2d(ch, njoints, kernel_size=1, bias=True))
            mask.append(nn.Conv2d(ch, 1, kernel_size=1, bias=True))

            if i < nstacks-1:
                _fc1.append(nn.Conv2d(ch, ch, kernel_size=1, bias=False))
                _fc2.append(nn.Conv2d(ch, ch, kernel_size=1, bias=False))
                _hm.append(nn.Conv2d(njoints, ch, kernel_size=1, bias=False))
                _mask.append(nn.Conv2d(1, ch, kernel_size=1, bias=False))

        self.hg2b  = nn.ModuleList(hg2b) # hgs: hourglass stack
        self.res1  = nn.ModuleList(res1)
        self.fc1   = nn.ModuleList(fc1)
        self._fc1  = nn.ModuleList(_fc1)
        self.res2  = nn.ModuleList(res2)
        self.fc2   = nn.ModuleList(fc2)
        self._fc2  = nn.ModuleList(_fc2)
        self.hm   = nn.ModuleList(hm)
        self._hm  = nn.ModuleList(_hm)
        self.mask  = nn.ModuleList(mask)
        self._mask = nn.ModuleList(_mask)


    def _make_fc(self, in_planes, out_planes):
        bn = nn.BatchNorm2d(in_planes)
        conv = nn.Conv2d(in_planes, out_planes, kernel_size=1, bias=False)
        return nn.Sequential(conv, bn, self.relu)


    def _make_residual(self, block, nblocks, in_planes, out_planes):
        layers = []
        layers.append( block( in_planes, out_planes) )
        self.in_planes = out_planes
        for i in range(1, nblocks):
            layers.append(block( self.in_planes, out_planes))
        return nn.Sequential(*layers)

    def forward(self, x):

        l_hm, l_mask, l_enc = [], [], []
        x = self.conv1(x) # x: (N,64,128,128)
        x = self.bn1(x)
        x = self.relu(x)

        x = self.layer1(x)
        x = self.maxpool(x) # x: (N,128,64,64)
        x = self.layer2(x)
        x = self.layer3(x)

        for i in range(self.nstacks): #2
            y_1, y_2, _ = self.hg2b[i](x)

            y_1 = self.res1[i](y_1)
            y_1 = self.fc1[i](y_1)
            est_hm = self.hm[i](y_1)
            l_hm.append(est_hm)

            y_2 = self.res2[i](y_2)
            y_2 = self.fc2[i](y_2)
            est_mask = self.mask[i](y_2)
            l_mask.append(est_mask)

            if i < self.nstacks-1:
                _fc1 = self._fc1[i](y_1)
                _hm = self._hm[i](est_hm)
                _fc2 = self._fc2[i](y_2)
                _mask = self._mask[i](est_mask)
                x = x + _fc1 + _fc2 + _hm + _mask
                l_enc.append(x)
            else:
                l_enc.append(x + y_1 + y_2)
        assert len(l_hm) == self.nstacks
        return l_hm, l_mask, l_enc
