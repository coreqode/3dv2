import glob
import cv2
from tqdm import tqdm, trange
import random
import torch
import torchvision
import torch.utils.data
import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
from PIL import Image, ImageFilter
import numpy as np
import pickle
from progress.bar import Bar
from termcolor import colored, cprint
from collections import defaultdict
from common import *

class HandDataset(torch.utils.data.Dataset):
    def __init__(self, data_split = 'test'):
        self.data_split = data_split
        self.njoints = 21
        self.inp_res = 256
        self.hm_res = 64
        self.depth_res = 64
        self.sigma = 2

        # Augmenation Parameters #
        self.blur_radius = 0.5
        self.brightness =  0.5
        self.saturation = 0.5
        self.hue = 0.5
        self.contrast = 0.5
        self.center_jittering = 0.1
        self.scale_jittering = 0.1
        self.max_rot = np.pi

        self.ref_bone_link = (0, 9)
        self.joint_root_idx = 9

        if data_split == 'test':
            with open('.cache/test.pkl', 'rb') as fi:
                self.annotations = pickle.load(fi)
                fi.close()
        else:
            with open('.cache/train.pkl', 'rb') as fi:
                self.annotations = pickle.load(fi)
                fi.close()


    def __getitem__(self, idx):
        rng = np.random.RandomState(seed=random.randint(0, 1024))
        image_path = self.annotations['color_path'][idx]
        depth_path = self.annotations['depth_path'][idx]
        mask_path = self.annotations['mask_path'][idx]
        img = Image.open(image_path).convert('RGB')
        kp2d = self.annotations['uv_vis'][idx]
        kp2d = kp2d[:21, :2]
        side = self.annotations['side'][idx]
        depth = self.get_depth(side, depth_path, mask_path)
        mask = Image.open(mask_path).convert('RGB')
        intr = self.annotations['K'][idx]
        center = self.annotations['center'][idx]
        scale = self.annotations['scale'][idx]
        joint = self.annotations['xyz'][idx]

        if side == 'left':
            flip = True
        else:
            flip = False
        if flip:
            img = img.transpose(Image.FLIP_LEFT_RIGHT)
            kp2d[..., 0] = img.size[0] - kp2d[..., 0]
            center[..., 0] = img.size[0] - center[..., 0]
            joint[..., 0] = -joint[..., 0]

        joint = joint[ :21]
        if self.data_split == 'train':
            center_offsets = ( self.center_jittering * scale * rng.uniform(low=-1, high=1, size=2))
            center = center + center_offsets.astype(int)

            scale_jittering = self.scale_jittering * rng.randn() + 1
            scale_jittering = np.clip( scale_jittering, 1 - self.scale_jittering, 1 + self.scale_jittering,)
            scale = scale * scale_jittering
            rot = rng.uniform(low=-self.max_rot, high=self.max_rot)

            blur_radius = random.random() * self.blur_radius
            img = img.filter(ImageFilter.GaussianBlur(blur_radius))
            img = self.color_jitter(img)
        else:
            rot = 0

        rot_mat = np.array([
            [np.cos(rot), -np.sin(rot), 0],
            [np.sin(rot),  np.cos(rot),  0],
            [0,             0,          1], ]).astype(np.float32)

        affinetrans, post_rot_trans = self.get_affine_transform(
            center=center,
            scale=scale,
            optical_center=[intr[0, 2], intr[1, 2]],  # (cx, cy)print(intr[0, 0])
            out_res=[self.inp_res, self.inp_res],
            rot=rot)

        kp2d = self.transform_coords(kp2d, affinetrans)
        new_intr = post_rot_trans.dot(intr)

        if self.data_split == 'train':
            joint = rot_mat.dot(joint.transpose(1, 0)).transpose()

        img = self.transform_img(img, affinetrans, [self.inp_res, self.inp_res])
        img = img.crop((0, 0, self.inp_res, self.inp_res))
        img = np.array(img)

        depth = self.transform_img(depth, affinetrans, [self.inp_res, self.inp_res])
        depth = depth.crop((0, 0, self.inp_res, self.inp_res))
        depth = self.normalize_depth(depth, joint_z = joint[:, 2])
        depth = depth * 256
        depth = cv2.resize(depth, (self.depth_res, self.depth_res))

        mask = depth.copy()
        np.putmask(mask, mask > 1e-2, 1.0)
        np.putmask(mask, mask <= 1e-2, 0.0)

        joint_bone = 0
        for jid, nextjid in zip(self.ref_bone_link[:-1], self.ref_bone_link[1:]):
            joint_bone += np.linalg.norm(joint[nextjid] - joint[jid])

        joint_root = joint[self.joint_root_idx]
        joint_bone = np.atleast_1d(joint_bone)
        jointR = joint - joint_root[np.newaxis, :]
        jointRS = jointR / joint_bone

        kin_chain = [ jointRS[i] - jointRS[SNAP_PARENT[i]] for i in range(21) ]
        kin_chain = np.array(kin_chain[1:])  # id 0's parent is itself
        kin_len = np.linalg.norm( kin_chain, ord=2, axis=-1, keepdims=True)
        kin_chain = kin_chain / kin_len

        hm = np.zeros( (self.njoints, self.hm_res, self.hm_res), dtype='float32')
        hm_veil = np.ones(self.njoints, dtype='float32')

        for j in range(self.njoints):
            kp = ( (kp2d[j] / self.inp_res) * self.hm_res).astype(np.int32)
            hm[j], aval = self.gen_heatmap(hm[j], kp, self.sigma)
            hm_veil[j] *= aval

        img = img / 127.5 - 1
        scale = np.array(scale)
        center = np.array(center)
        img = torch.from_numpy(img).float().permute(2, 0, 1)
        hm = torch.from_numpy(hm).float()
        hm_veil = torch.from_numpy(hm_veil).float()
        depth = torch.from_numpy(depth).float()
        mask = torch.from_numpy(mask).float()
        center = torch.from_numpy(center).float()
        scale = torch.from_numpy(scale).float()
        joint = torch.from_numpy(joint).float()
        intr = torch.from_numpy(intr).float()
        jointRS = torch.from_numpy(jointRS).float()
        jointR = torch.from_numpy(jointR).float()
        joint_bone = torch.from_numpy(joint_bone).float()
        joint_root = torch.from_numpy(joint_root).float()
        model_inputs = {'image': img,
                        'intr' : intr}
        data = {'image':img,
                'depth': depth,
                'mask': mask,
                'center': center,
                'scale': scale,
                'joint': joint,
                'intr': new_intr,
                'jointRS': jointRS,
                'jointR': jointR,
                'joint_bone': joint_bone,
                'joint_root': joint_root,
                'heatmap': hm,
                'heatmap_veil': hm_veil}
        return model_inputs, data

    def get_depth(self,side, depth_path, mask_path):
        id_left = [i for i in range(2, 18)]
        id_right = [i for i in range(18, 34)]

        if side == 'left':
            valid_mask_id = id_left
        else:
            valid_mask_id = id_right

        depth = Image.open(depth_path).convert('RGB')
        mask = Image.open(mask_path).convert('RGB')
        depth = np.array(depth)
        mask = np.array(mask)[..., 2:]
        ll = valid_mask_id[0]
        uu = valid_mask_id[-1]
        mask[mask < ll] = 0
        mask[mask > uu] = 0
        mask[mask > 0] = 1
        if mask.dtype != np.uint8:
            mask = mask.astype(np.uint8)
        depth = np.multiply(depth, mask)
        depth = Image.fromarray(depth, mode="RGB")
        return depth

    def gen_heatmap(self, img, pt, sigma):
        pt = pt.astype(np.int32)
        # Check that any part of the gaussian is in-bounds
        ul = [int(pt[0] - 3 * sigma), int(pt[1] - 3 * sigma)]
        br = [int(pt[0] + 3 * sigma + 1), int(pt[1] + 3 * sigma + 1)]
        if ( ul[0] >= img.shape[1] or ul[1] >= img.shape[0] or br[0] < 0 or br[1] < 0):
            return img, 0

        # Generate gaussian
        size = 6 * sigma + 1
        x = np.arange(0, size, 1, float)
        y = x[:, np.newaxis]
        x0 = y0 = size // 2
        g = np.exp(- ((x - x0) ** 2 + (y - y0) ** 2) / (2 * sigma ** 2))
        # Usable gaussian range
        g_x = max(0, -ul[0]), min(br[0], img.shape[1]) - ul[0]
        g_y = max(0, -ul[1]), min(br[1], img.shape[0]) - ul[1]
        # Image range
        img_x = max(0, ul[0]), min(br[0], img.shape[1])
        img_y = max(0, ul[1]), min(br[1], img.shape[0])

        img[img_y[0]:img_y[1], img_x[0]:img_x[1]] = g[g_y[0]:g_y[1], g_x[0]:g_x[1]]
        return img, 1

    def transform_img(self, img, affine_trans, res):
        """
        Args:
        center (tuple): crop center coordinates
        scale (int): size in pixels of the final crop
        res (tuple): final image size
        """
        trans = np.linalg.inv(affine_trans)

        img = img.transform(
            tuple(res), Image.AFFINE, (trans[0, 0], trans[0, 1], trans[0, 2],
                                       trans[1, 0], trans[1, 1], trans[1, 2])
        )
        return img

    def color_jitter(self, img,):
        brightness = self.brightness
        saturation = self.saturation
        hue = self.hue
        contrast = self.contrast

        brightness, contrast, saturation, hue = self.get_color_params( brightness=brightness, contrast=contrast, saturation=saturation, hue=hue)

        # Create img transform function sequence
        img_transforms = []
        if brightness is not None:
            img_transforms.append(lambda img: torchvision.transforms.functional.adjust_brightness(img, brightness))
        if saturation is not None:
            img_transforms.append(lambda img: torchvision.transforms.functional.adjust_saturation(img, saturation))
        if hue is not None:
            img_transforms.append(
                lambda img: torchvision.transforms.functional.adjust_hue(img, hue))
        if contrast is not None:
            img_transforms.append(lambda img: torchvision.transforms.functional.adjust_contrast(img, contrast))
        random.shuffle(img_transforms)

        jittered_img = img
        for func in img_transforms:
            jittered_img = func(jittered_img)
        return jittered_img

    def get_color_params(self, brightness=0, contrast=0, saturation=0, hue=0):
        if brightness > 0:
            brightness_factor = random.uniform(
                max(0, 1 - brightness), 1 + brightness)
        else:
            brightness_factor = None

        if contrast > 0:
            contrast_factor = random.uniform(max(0, 1 - contrast), 1 + contrast)
        else:
            contrast_factor = None

        if saturation > 0:
            saturation_factor = random.uniform(
                max(0, 1 - saturation), 1 + saturation)
        else:
            saturation_factor = None

        if hue > 0:
            hue_factor = random.uniform(-hue, hue)
        else:
            hue_factor = None
        return brightness_factor, contrast_factor, saturation_factor, hue_factor

    def __len__(self):
        return len(self.annotations['color_path'])

    def get_affine_transform(self, center, scale, optical_center, out_res, rot=0):
        rot_mat = np.zeros((3, 3))
        sn, cs = np.sin(rot), np.cos(rot)
        rot_mat[0, :2] = [cs, -sn]
        rot_mat[1, :2] = [sn, cs]
        rot_mat[2, 2] = 1
        # Rotate center to obtain coordinate of center in rotated image
        origin_rot_center = rot_mat.dot(center.tolist() + [1])[:2]
        # Get center for transform with verts rotated around optical axis
        # (through pixel center, smthg like 128, 128 in pixels and 0,0 in 3d world)
        # For this, rotate the center but around center of image (vs 0,0 in pixel space)
        t_mat = np.eye(3)
        t_mat[0, 2] = - optical_center[0]
        t_mat[1, 2] = - optical_center[1]
        t_inv = t_mat.copy()
        t_inv[:2, 2] *= -1
        transformed_center = (
            t_inv.dot(rot_mat).dot(t_mat).dot(center.tolist() + [1])
        )
        post_rot_trans = self.get_affine_trans_no_rot(origin_rot_center, scale, out_res)
        total_trans = post_rot_trans.dot(rot_mat)
        # check_t = get_affine_transform_bak(center, scale, res, rot)
        # print(total_trans, check_t)
        affinetrans_post_rot = self.get_affine_trans_no_rot(
            transformed_center[:2], scale, out_res
        )
        return ( total_trans.astype(np.float32), affinetrans_post_rot.astype(np.float32),)

    def get_affine_trans_no_rot(self, center, scale, res):
        affinet = np.zeros((3, 3))
        affinet[0, 0] = float(res[1]) / scale
        affinet[1, 1] = float(res[0]) / scale
        affinet[0, 2] = res[1] * (-float(center[0]) / scale + .5)
        affinet[1, 2] = res[0] * (-float(center[1]) / scale + .5)
        affinet[2, 2] = 1
        return affinet

    def transform_coords(self, pts, affine_trans, invert=False):
        """
        Args:
            pts(np.ndarray): (point_nb, 2)
        """
        if invert:
            affine_trans = np.linalg.inv(affine_trans)
        hom2d = np.concatenate([pts, np.ones([np.array(pts).shape[0], 1])], 1)
        transformed_rows = affine_trans.dot(hom2d.transpose()).transpose()[:, :2]
        return transformed_rows.astype(int)

    def normalize_depth(self, dep_, joint_z):
        """RHD depthmap to depth image
        :param dm: depth map, RGB, R * 255 + G
        :type dm: np (H, W, 3)
        :param dm_mask: depth mask
        :type dm_mask: np (H, W, 3)
        :param hand_flag: 'l':left, 'r':right
        :type hand_flag: str
        :return: scaled dep image
        :rtype: np (H, W), a 0~1 float reptesent depth
        """
        if isinstance(dep_, PIL.Image.Image):
            dep_ = np.array(dep_)
            assert (dep_.shape[-1] == 3)  # used to be "RGB"

        ''' Converts a RGB-coded depth into float valued depth. '''
        dep = (dep_[:, :, 0] * 2 ** 8 + dep_[:, :, 1]).astype('float32')
        dep /= float(2 ** 16 - 1)
        dep *= 5.0  ## depth in meter !
        lower_bound = joint_z.min() - 0.05  # m
        upper_bound = joint_z.max() + 0.05

        np.putmask(dep, dep <= lower_bound, upper_bound)
        min_dep = dep.min() - 1e-3  # slightly compensate
        np.putmask(dep, dep >= upper_bound, 0.0)
        max_dep = dep.max() + 1e-3
        np.putmask(dep, dep <= min_dep, max_dep)
        range_dep = max_dep - min_dep
        dep = (-1 * dep + max_dep) / range_dep
        return dep

if __name__== '__main__':
    dt = HandDataset()
    _ = dt[10]
