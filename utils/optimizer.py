import torch


def Adam(param, learning_rate):
    return torch.optim.Adam(param, lr=learning_rate, betas=(0.9, 0.999), eps=1e-08, weight_decay=0, amsgrad=False)