import sys
sys.path.append('../')

import torch
import torch.nn as nn
from backbones.bottleneck import BottleneckBlock

class ResNet18(nn.Module):
    def __init__(self, block=BottleneckBlock, out_plane=256):
        super(ResNet18, self).__init__()
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_residual(block, 2, 256, 512)
        self.layer2 = self._make_residual(block, 2, 512, 512)
        self.layer3 = self._make_residual(block, 2, 512, 512)
        self.layer4 = self._make_residual(block, 2, 512, 512)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(
                    m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def _make_residual(self, block, nblocks, in_planes, out_planes):
        layers = []
        layers.append(block(in_planes, out_planes))
        self.in_planes = out_planes
        for i in range(1, nblocks):
            layers.append(block(self.in_planes, out_planes))
        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.maxpool(x)  # 32
        x = self.layer1(x)
        x = self.maxpool(x)  # 16
        x = self.layer2(x)
        x = self.maxpool(x)  # 8
        x = self.layer3(x)
        x = self.maxpool(x)  # 4
        x = self.layer4(x)
        x = self.avgpool(x)  # 1
        x = torch.flatten(x, 1)

        return x
